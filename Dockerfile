FROM alpine:3.16

RUN addgroup -g 1000 anon && \
    adduser -D -h /home/anon -G anon -s /bin/ash -u 1000 anon anon && \
    \
    case $(uname -m) in \
        "aarch64" | "arm64") \
            ARCH="arm64" \
            ;; \
        *) \
            ARCH="amd64" \
            ;; \
    esac && \
    \
    apk update && \
    apk add curl sudo && \
    curl -sSL -o "mkcert" "https://dl.filippo.io/mkcert/latest?for=linux/${ARCH}" && \
    chmod +x mkcert && \
    cp mkcert /usr/local/bin/ && \
    echo "Installed mkcert $(mkcert --version)"

USER anon

ENV CAROOT=/home/anon

WORKDIR /home/anon

ENTRYPOINT [ "mkcert", "-cert-file", "public-key.pem", "-key-file", "private-key.pem" ]
